@extends('template1')

@section('content')
<!-- Start Banner Hero -->
<div id="template-mo-zay-hero-carousel" class="carousel slide" data-bs-ride="carousel">
        <ol class="carousel-indicators">
            <li data-bs-target="#template-mo-zay-hero-carousel" data-bs-slide-to="0" class="active"></li>
            <li data-bs-target="#template-mo-zay-hero-carousel" data-bs-slide-to="1"></li>
            <li data-bs-target="#template-mo-zay-hero-carousel" data-bs-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <div class="container">
                    <div class="row p-5">
                        <div class="mx-auto col-md-8 col-lg-6 order-lg-last">
                            <img class="img-fluid" src="img/guantes.jpeg" alt="">
                        </div>
                        <div class="col-lg-6 mb-0 d-flex align-items-center">
                            <div class="text-align-left align-self-center">
                                <h1 class="h1 text-success"> Guantes industriales <b>Wario</b></h1>
                                <h3 class="h2">Guantes</h3>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="container">
                    <div class="row p-5">
                        <div class="mx-auto col-md-8 col-lg-6 order-lg-last">
                            <img class="img-fluid" src="img/antebraceras.jpeg" alt="">
                        </div>
                        <div class="col-lg-6 mb-0 d-flex align-items-center">
                            <div class="text-align-left">
                                <h1 class="h1 text-success"> Guantes industriales <b>Wario</b></h1>
                                <h3 class="h2">Antebraceras</h3>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="container">
                    <div class="row p-5">
                        <div class="mx-auto col-md-8 col-lg-6 order-lg-last">
                            <img class="img-fluid" src="img/pecheras.jpeg" alt="">
                        </div>
                        <div class="col-lg-6 mb-0 d-flex align-items-center">
                            <div class="text-align-left">
                                <h1 class="h1 text-success"> Guantes industriales <b>Wario</b></h1>
                                <h3 class="h2">Pecheras</h3>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="container">
                    <div class="row p-5">
                        <div class="mx-auto col-md-8 col-lg-6 order-lg-last">
                            <img class="img-fluid" src="img/chamarras.jpg" alt="">
                        </div>
                        <div class="col-lg-6 mb-0 d-flex align-items-center">
                            <div class="text-align-left">
                                <h1 class="h1 text-success"> Guantes industriales <b>Wario</b></h1>
                                <h3 class="h2">Chamarras</h3>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="container">
                    <div class="row p-5">
                        <div class="mx-auto col-md-8 col-lg-6 order-lg-last">
                            <img class="img-fluid" src="img/polainas.jpg" alt="">
                        </div>
                        <div class="col-lg-6 mb-0 d-flex align-items-center">
                            <div class="text-align-left">
                                <h1 class="h1 text-success"> Guantes industriales <b>Wario</b></h1>
                                <h3 class="h2">Polainas</h3>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev text-decoration-none w-auto ps-3" href="#template-mo-zay-hero-carousel" role="button" data-bs-slide="prev">
            <i class="fas fa-chevron-left"></i>
        </a>
        <a class="carousel-control-next text-decoration-none w-auto pe-3" href="#template-mo-zay-hero-carousel" role="button" data-bs-slide="next">
            <i class="fas fa-chevron-right"></i>
        </a>
    </div>
    <!-- End Banner Hero -->


    <!-- Start Categories of The Month -->
    <section class="container py-5">
        <div class="row text-center pt-3">
            <div class="col-lg-6 m-auto">
                <h1 class="h1">Categorías de los productos disponibles</h1>
                <p>
                    Navega dentro de las categorías y encuentra lo que estás buscando.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-4 p-5 mt-3">
                <a href="#"><img src="img/guantes.jpeg" class="rounded-circle img-fluid border"></a>
                <h5 class="text-center mt-3 mb-3">GUANTES</h5>
                <p class="text-center"><a href="tienda" class="btn btn-success">Ir a la tienda</a></p>
            </div>
            <div class="col-12 col-md-4 p-5 mt-3">
                <a href="#"><img src="img/antebraceras.jpeg" class="rounded-circle img-fluid border"></a>
                <h2 class="h5 text-center mt-3 mb-3">ANTEBRACERAS</h2>
                <p class="text-center"><a href="tienda" class="btn btn-success">Ir a la tienda</a></p>
            </div>
            <div class="col-12 col-md-4 p-5 mt-3">
                <a href="#"><img src="img/pecheras.jpeg" class="rounded-circle img-fluid border"></a>
                <h2 class="h5 text-center mt-3 mb-3">PECHERAS</h2>
                <p class="text-center"><a href="tienda" class="btn btn-success">Ir a la tienda</a></p>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-2 p-5 mt-3">
            </div>
            <div class="col-12 col-md-4 p-5 mt-3">
                <a href="#"><img src="img/chamarras.jpg" class="rounded-circle img-fluid border"></a>
                <h5 class="text-center mt-3 mb-3">CHAMARRAS</h5>
                <p class="text-center"><a href="tienda" class="btn btn-success">Ir a la tienda</a></p>
            </div>
            <div class="col-12 col-md-4 p-5 mt-3">
                <a href="#"><img src="img/polainas.jpg" class="rounded-circle img-fluid border"></a>
                <h2 class="h5 text-center mt-3 mb-3">POLAINAS</h2>
                <p class="text-center"><a href="tienda" class="btn btn-success">Ir a la tienda</a></p>
            </div>
            <div class="col-12 col-md-2 p-5 mt-3">
            </div>
        </div>
    </section>
    <!-- End Categories of The Month -->
@endsection
