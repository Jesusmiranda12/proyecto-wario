@extends('template1')

@section('content')
<!-- Start Content -->
<section class="container py-5">
        <div class="row">

            <div class="col-lg-3">
                <h1 class="h2 pb-4">Categorías</h1>
                <ul class="list-unstyled templatemo-accordion">
                    
                    <div class="mdl-textfield mdl-js-textfield">
                    @foreach($categorias as $categoria)
                    <div class="row">
                        <div class="col-lg-10">
                        <p><b>{{$categoria->nombre}}</b></p>
                        </div>
                        <div class="col-lg-2">
                        <form action="{{route('tienda.index',$categoria->nombre)}}">
                        <input type="hidden" name="categoria" value="{{$categoria->nombre}}">
                        <button type="submit" class="btn btn-success"><i class="fas fa-eye"></i></button>
                        </form>
                        </div>
                    </div>
                    @endforeach
                    
                </ul> 
                
            </div>

            <div class="col-lg-9">
                <div class="row">
                    <div class="col-md-6">
                        <input type="search"><i class="fas fa-search"></i>
                    </div>
                    <div class="col-md-6 pb-4">
                        
                    </div>
                </div>
                <div class="row">
                @foreach($productos as $producto)
                    <div class="col-md-4">       
                        <div class="card mb-4 product-wap rounded-0">
                            <div class="card rounded-0">
                                <img class="card-img rounded-0 img-fluid" src="img/Productos/{{$producto->imagen}}">
                                <div class="card-img-overlay rounded-0 product-overlay d-flex align-items-center justify-content-center">
                                    <ul class="list-unstyled">
                                        <li><a class="btn btn-success text-white mt-2" href="shop-single.html"><i class="far fa-eye"></i></a></li>
                                        <li><a class="btn btn-success text-white mt-2" href="shop-single.html"><i class="fas fa-cart-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-body">
                                <a href="shop-single.html" class="h3 text-decoration-none">{{$producto->nombreP}}</a>
                                <ul class="w-100 list-unstyled d-flex justify-content-between mb-0">
                                    <li>Tallas: {{$producto->talla}}</li>
                                    
                                </ul>
                                
                                <p class="text-right mb-0">{{$producto->precio}}</p>
                                <div class="col-auto">
                                        <ul class="list-inline pb-3">
                                            <li class="list-inline-item text-center">
                                                Cantidad
                                                <input type="hidden" name="product-quanity" id="product-quanity" value="1">
                                            </li>
                                            <li class="list-inline-item"><span class="btn btn-success" id="btn-minus">-</span></li>
                                            <li class="list-inline-item"><span class="badge bg-secondary" id="var-value">1</span></li>
                                            <li class="list-inline-item"><span class="btn btn-success" id="btn-plus">+</span></li>
                                        </ul>
                                </div>
                            </div>
                        </div>  
                    </div>
                @endforeach
                          <div div="row">
                    <ul class="pagination pagination-lg justify-content-end">
                        <li class="page-item disabled">
                            <a class="page-link active rounded-0 mr-3 shadow-sm border-top-0 border-left-0" href="#" tabindex="-1">1</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link rounded-0 mr-3 shadow-sm border-top-0 border-left-0 text-dark" href="#">2</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link rounded-0 shadow-sm border-top-0 border-left-0 text-dark" href="#">3</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- End Content -->
@endsection