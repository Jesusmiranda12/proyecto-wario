@extends('template1')

@section('content')
<section class="container py-5">
        <table class="table table-responsive table-striped table-bordered">
            <thead>
                <tr>
                    
                    <th>Nombre</th>
                    <th>Precio</th>
                    <th>Cantidad</th>
                    <th>Subtotal</th>
                    <th>Acción</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    
                    <td>Pechera industrial</td>
                    <td>80.00</td>
                    <td>2</td>
                    <td>160.00</td>
                    <td><button type="submit" class="btn btn-danger btn-lg px-3">Eliminar</button></td>
                </tr>
                
            </tbody>
        </table>
        <div class="row">
            <div class="col text-end mt-2">
                <button type="submit" class="btn btn-outline-primary btn-lg px-3">Generar PDF</button>
            </div>
        </div>
    </section>
    <!-- End Section -->
    <!-- Start Contact -->
    <div class="container py-5 bg-light">
                  
            <form action="{{route('clientes.store')}}" class="col-md-9 m-auto" method="post" role="form">
                @csrf
                <p>Llena los siguientes datos y envía tu cotización con el archivo generado.</p>
                <div class="row">                  
                    <div class="form-group col-md-6 mb-3">
                        <label for="inputname">Nombre</label>
                        <input type="text" class="form-control mt-1" id="name" name="nombreC" placeholder="Nombre">
                    </div>
                    <div class="form-group col-md-6 mb-3">
                        <label for="inputemail">E-mail</label>
                        <input type="email" class="form-control mt-1" id="email" name="email" placeholder="E-mail">
                    </div>
                </div>
                <div class="mb-3">
                    <label for="inputsubject">Razón social</label>
                    <input type="text" class="form-control mt-1" id="subject" name="razon" placeholder="Razón Social">
                </div>
                <div class="mb-3">
                    <label for="inputsubject">RFC</label>
                    <input type="text" class="form-control mt-1" id="subject" name="rfc" placeholder="RFC">
                </div>
                <div class="mb-3">
                    <label for="inputsubject">Dirección</label>
                    <input type="text" class="form-control mt-1" id="subject" name="direccion" placeholder="Dirección">
                </div>
                <div class="mb-3">
                    <label for="inputsubject">Teléfono</label>
                    <input type="text" class="form-control mt-1" id="subject" name="telefono" placeholder="Teléfono">
                </div>
                <div class="row">
                    <div class="col text-end mt-2">
                        <button type="submit" class="btn btn-success btn-lg px-3">Envíar</button>
                    </div>
                </div>
            </form>
        
    </div>
    <!-- End Contact -->
@endsection