@extends('template1')

@section('content')
<section class="bg-success py-5">
        <div class="container">
            <div class="row align-items-center py-5">
                <div class="col-md-8 text-white">
                    <h1 class="text-center">Acerca de nosotros</h1>
                    <p class="text-center">
                    Somos fabricantes con más de 25 años de experiencia en la manufactura de equipo de protección industrial, con presencia en todo el país.

Guantes Industriales Wario nos especializamos en la fabricación de equipo de protección personal de calidad a un precio justo en materiales como carnaza, piel, mezclilla, aluminio, kevlar, entre otros.

Somos la mejor opción en precio, calidad y servicio tanto para grandes o pequeñas empresas.
                    </p>
                </div>
                <div class="col-md-4">
                    <img src="img/logo.jpeg" alt="About Hero">
                </div>
            </div>
        </div>
    </section>
    <!-- Close Banner -->

    <!-- Start Section -->
    <section class="container py-5">
        <div class="row text-center pt-5 pb-3">
            <div class="col-lg-6 m-auto">
                <h1 class="h1">Conocenos un poco más</h1>
                <p>
                    Misión, Visión y Valores de nuestra organización.
                </p>
            </div>
        </div>
        <div class="row">

            <div class="col-md-6 col-lg-4 pb-5">
                <div class="h-100 py-5 services-icon-wap shadow">
                    <div class="h1 text-success text-center"><i class="fas fa-book-open"></i></div>
                    <h2 class="h5 mt-4 text-center">Misión</h2>
                    <p class="text-center">
                        OFRECER A NUESTROS CLIENTES PRODUCTOS DE SEGURIDAD INDUSTRIAL, DISEÑADOS Y DESARROLLADOS PARA SATISFACER NECESIDADES Y EXPECTATIVAS A PRECIO JUSTO Y EXCELENTE SERVICIO.
                    </p>
                </div>
            </div>

            <div class="col-md-6 col-lg-4 pb-5">
                <div class="h-100 py-5 services-icon-wap shadow">
                    <div class="h1 text-success text-center"><i class="fas fa-eye"></i></div>
                    <h2 class="h5 mt-4 text-center">Visión</h2>
                    <P class="text-center"> 
                        SER UNA EMPRESA DESTACADA POR SU INNOVACIÓN Y CALIDAD DENTRO DEL SECTOR INDUSTRIAL, CUBRIENDO LAS NECESIDADES DE NUESTROS CLIENTES, LOGRANDO DE ESTA MANERA UN ALTO NIVEL DE PRODUCTIVIDAD Y RENTABILIDAD PARA CONSOLIDARNOS COMO LÍDERES DENTRO DEL MERCADO.
                    </P>
                </div>
            </div>

            <div class="col-md-6 col-lg-4 pb-5">
                <div class="h-100 py-5 services-icon-wap shadow">
                    <div class="h1 text-success text-center"><i class="fas fa-handshake"></i></div>
                    <h2 class="h5 mt-4 text-center">Valores</h2>
                    <P class="text-center">
                        COMPROMISO <BR> SERIEDAD <BR> HONESTIDAD <BR> RESPONSABILIDAD
                    </P>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section -->

    <!-- Start Brands --> <!--Me gustaría poner un Modal aquí para el contenido de lo anterior-->
    
    <!--End Brands-->
@endsection