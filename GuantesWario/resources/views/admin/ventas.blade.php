@extends('template2')

@section('contenido')
<section class="full-width pageContent">
		<section class="full-width header-well">
			<div class="full-width header-well-icon">
				<i class="zmdi zmdi-shopping-cart"></i>
			</div>
			<div class="full-width header-well-text">
				<p class="text-condensedLight">
					Registra las ventas realizadas y Consulta los detalles de la venta.
				</p>
			</div>
		</section>
		<div class="full-width divider-menu-h"></div>
		<div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
			<div class="mdl-tabs__tab-bar">
				<a href="#tabNewVenta" class="mdl-tabs__tab is-active">Registrar Venta</a>
				<a href="#tabListVentas" class="mdl-tabs__tab">Consultar Ventas</a>
			</div>
			<div class="mdl-tabs__panel is-active" id="tabNewVenta">
				<div class="mdl-grid">
					<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--12-col-desktop">
						<div class="full-width panel mdl-shadow--2dp">
							<div class="full-width panel-tittle bg-primary text-center tittles">
								Registrar venta
							</div>
							<div class="full-width panel-content">
								<form action="{{route('ventas.store')}}" method="post" enctype="multipart/form-data">
								@csrf
									<div class="mdl-grid">
										<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--6-col-desktop">
											<h5 class="text-condensedLight">Información básica</h5>
											
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
												<input class="mdl-textfield__input"  id="fechaV" name="fechaVenta">
												<label class="mdl-textfield__label" for="fechaV">Fecha</label>
												<span class="mdl-textfield__error">Fecha no valida</span>
											</div>
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
												<input class="mdl-textfield__input" type="text" pattern="-?[A-Za-z0-9áéíóúÁÉÍÓÚ ]*(\.[0-9]+)?" id="facturaV" name="facturaVenta">
												<label class="mdl-textfield__label" for="facturaV">Número de factura</label>
												<span class="mdl-textfield__error">Dato incorrecto</span>
											</div>
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
												<input class="mdl-textfield__input" type="text" pattern="-?[A-Za-z0-9áéíóúÁÉÍÓÚ ]*(\.[0-9]+)?" id="razonV" name="razonVenta">
												<label class="mdl-textfield__label" for="razonV">Razón Social</label>
												<span class="mdl-textfield__error">Dato incorrecto</span>
											</div>
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
												<input class="mdl-textfield__input" type="text" pattern="-?[A-Za-z0-9áéíóúÁÉÍÓÚ ]*(\.[0-9]+)?" id="numeroV" name="numeroVenta">
												<label class="mdl-textfield__label" for="numeroV">Número de guía</label>
												<span class="mdl-textfield__error">Datos no válidos</span>
											</div>
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
												<label class="mdl-textfield__label" for="descripcionV">Descripción</label>
												<textarea class="mdl-textfield__input" type="text" pattern="-?[A-Za-z0-9áéíóúÁÉÍÓÚ ]*(\.[0-9]+)?" id="descripcionV" name="descripcionVenta">
												</textarea>
												
												<span class="mdl-textfield__error">Dato incorrecto</span>
											</div>
												
										</div>
										<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--6-col-desktop">
											<h5 class="text-condensedLight">Archivo de la guía</h5>
											<div class="mdl-textfield mdl-js-textfield">
												<input type="file" name="archivoguiaVenta">
											</div>
											<h5 class="text-condensedLight">Archivo de factura</h5>
											<div class="mdl-textfield mdl-js-textfield">
												<input type="file" name="archivofacturaVenta">
											</div>
										</div>
									</div>
									<p class="text-center">
										<button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored bg-primary" id="btn-addProduct">
											<i class="zmdi zmdi-plus"></i>
										</button>
										<div class="mdl-tooltip" for="btn-addProduct">Añadir producto</div>
									</p>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="mdl-tabs__panel" id="tabListVentas">
			<div class="mdl-grid">
			<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--12-col-desktop">
			   <form action="#">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
						<label class="mdl-button mdl-js-button mdl-button--icon" for="searchVenta">
							<i class="zmdi zmdi-search"></i>
						</label>
						<div class="mdl-textfield__expandable-holder">
							<input class="mdl-textfield__input" type="text" id="searchVenta">
							<label class="mdl-textfield__label"></label>
						</div>
					</div>
				</form>
				<table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp full-width table-responsive">
					<thead>
						<tr>
							<th class="mdl-data-table__cell--non-numeric">Fecha</th>
							<th>Razón Sócial</th>
							<th>Descripción</th>
							<th>Guía</th>
							<th>Factura</th>
							<th>Opciones</th>
						</tr>
					</thead>
					<tbody>
						@foreach($ventas as $venta)
						<tr>						
							<td>{{$venta->fecha}}</td>
							<td>{{$venta->razonsocial}}</td>
							<td>{{$venta->descripcion}}</td>
							<td data-Titulo="PDF">
                             <a href="pdf/guias/{{$venta->archivoguia}}" target="_blank">{{$venta->archivoguia}}</a>
                            </td>
							<td data-Titulo="PDF">
                             <a href="pdf/facturas/{{$venta->archivofactura}}" target="_blank">{{$venta->archivofactura}}</a>
                            </td>
							<td> 
							    <div class="row">
									<div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                    <a href="{{url('/ventas/'.$venta->id.'/edit')}}" class="btn text-white" style="background-color: #207671;"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-file-earmark-diff-fill" viewBox="0 0 16 16">
                                    <path d="M9.293 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V4.707A1 1 0 0 0 13.707 4L10 .293A1 1 0 0 0 9.293 0zM9.5 3.5v-2l3 3h-2a1 1 0 0 1-1-1zM8 6a.5.5 0 0 1 .5.5V8H10a.5.5 0 0 1 0 1H8.5v1.5a.5.5 0 0 1-1 0V9H6a.5.5 0 0 1 0-1h1.5V6.5A.5.5 0 0 1 8 6zm-2.5 6.5A.5.5 0 0 1 6 12h4a.5.5 0 0 1 0 1H6a.5.5 0 0 1-.5-.5z"/>
                                    </svg></a>
                            	</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                	<form action="{{url('/ventas/'.$venta->id)}}" class="formulario-eliminar" method="POST">
                                	@csrf  
                                	@method('DELETE')
                                	<button class="btn btn-danger" type="submit"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16">
                                	<path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
                                	</svg></button>
                                	</form>
                                </div>
								</div>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
			</div>
		</div>		
	</section>
<!-- Modal -->
<div class="modal fade" id="editarModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Actualizar datos</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
	  <div class="full-width panel-content">
		<form id="formularioEditar" enctype="multipart/form-data" method="post">
			@csrf
				<input type="hidden" id="idModal">
				<h5 class="text-condensedLight">Datos de la venta</h5>
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
				    <label  for="fechaModal">Fecha</label>	
				    <input class="mdl-textfield__input"  id="fechaModal" name="fechaVentaModal">		 			
		 			<span class="mdl-textfield__error">Fecha no valida</span>
		 		</div>
		 		<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
		 			 <label  for="facturaModal">Número de factura</label>	
					 <input class="mdl-textfield__input" type="text" pattern="-?[A-Za-z0-9áéíóúÁÉÍÓÚ ]*(\.[0-9]+)?" id="facturaModal" name="facturaVentaModal">	 			
		 			<span class="mdl-textfield__error">Dato incorrecto</span>
		 		</div>
		 		<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
					 <label for="razonModal">Razón Social</label>
					 <input class="mdl-textfield__input" type="text" pattern="-?[A-Za-z0-9áéíóúÁÉÍÓÚ ]*(\.[0-9]+)?" id="razonModal" name="razonVentaModal">	
		 			<span class="mdl-textfield__error">Dato incorrecto</span>
		 		</div>
		 		<div class="mdl-textfield mdl-js-textfield">
		 			<select class="mdl-textfield__input" id="productoModal" name="productoVentaModal">
		 				<option>Seleccionar producto</option>

		 				<option>opcion</option>

		 			</select>
		 		</div>
		 		<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
				 	<label  for="precioPVModal">Precio por producto</label>
		 			<input class="mdl-textfield__input" type="text" pattern="-?[0-9.]*(\.[0-9]+)?" id="precioPVModal" name="precioProductoVentaModal">	
		 			<span class="mdl-textfield__error">Precio inválido</span>
		 		</div>
		 		<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
				 	<label  for="cantidadModal">Cantidad vendida</label>
		 			<input class="mdl-textfield__input" type="text" pattern="-?[A-Za-z0-9áéíóúÁÉÍÓÚ ]*(\.[0-9]+)?" id="cantidadModal" name="cantidadVentaModal">	
		 			<span class="mdl-textfield__error">Cantidad inválida</span>
		 		</div>
		 		<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
				 	<label  for="totalModal">Total</label>
		 			<input class="mdl-textfield__input" type="text" pattern="-?[A-Za-z0-9áéíóúÁÉÍÓÚ ]*(\.[0-9]+)?" id="totalModal" name="totalVentaModal">	
		 			<span class="mdl-textfield__error">Dato no válido</span>
		 		</div>
		 		<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
				 	<label  for="guiaModal">Número de guía</label>
		 			<input class="mdl-textfield__input" type="text" pattern="-?[A-Za-z0-9áéíóúÁÉÍÓÚ ]*(\.[0-9]+)?" id="guiaModal" name="numeroGuiaModal">	
		 			<span class="mdl-textfield__error">Datos no válidos</span>
		 		</div>	
		 	</div>
		 	<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--6-col-desktop">
		 		<h5 class="text-condensedLight">Archivo de la guía</h5>
		 		<div class="mdl-textfield mdl-js-textfield">
		 			<input type="file" id="archivoguiaModal" name="archivoGuiaModal">
		 		</div>
		 		<h5 class="text-condensedLight">Archivo de factura</h5>
		 		<div class="mdl-textfield mdl-js-textfield">
		 			<input type="file" id="archivofacturaModal" name="archivoFacturaModal">
		 		</div>
		 	</div>				
				<button type="submit" class="btn btn-primary">Guardar Cambios</button>			
		</form>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>     
      </div>
    </div>
  </div>
</div>
@endsection
@section('javascript')
<script>
	function editar(id)
	{
		$(document).ready(function(){
            $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
            $.ajax({
            url:'/editar/ventas',
            method:'POST',
            data:{
            id:id
            } 
            }).done(function(respuesta){
				//alert(respuesta);
		    var arreglo = JSON.parse(respuesta);
			
			$("#idModal").val(arreglo[0].id);
			$("#fechaModal").val(arreglo[0].fecha);
			$("#facturaModal").val(arreglo[0].nofactura);
			$("#razonModal").val(arreglo[0].razonsocial);
			$("#productoModal").val(arreglo[0].producto);
			$("#precioPVModal").val(arreglo[0].precioproducto);
			$("#cantidadModal").val(arreglo[0].cantidad);
			$("#totalModal").val(arreglo[0].total);
			$("#guiaModal").val(arreglo[0].noguia);
			$("#archivoguiaModal").val(arreglo[0].archivoguia);
			$("#archivofacturaModal").val(arreglo[0].archivofactura);
            });
        });
	}
</script>
<script>
    $('#formularioEditar').submit(function(e){
		//alert('ok');
          e.preventDefault();
          $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        var idM = $('#idModal').val();
        var fechaM = $('#fechaModal').val();
        var nofacturaM = $('#facturaModal').val();
		var razonsocialM = $('#razonModal').val();
		var productoM = $('#productoModal').val();
		var precioproductoM = $('#precioPVModal').val();
		var cantidadM = $('#cantidadModal').val();
		var totalM = $('#totalModal').val();
		var noguiaM = $('#guiaModal').val();
		var archivoguiaM = $('#archivoguiaModal').val();
		var archivofacturaM = $('#archivofacturaModal').val();
		
        $.ajax({
          url:'/actualizar/ventas',
          method:'POST',
          data:{
            id:idM,
            fecha:fechaM,
			nofactura:nofacturaM,
			razonsocial:razonsocialM,
			producto:productoM,
			precioproducto:precioproductoM,
			cantidad:cantidadM,
			total:totalM,
			noguia:noguiaM,
			archivoguia:archivoguiaM,
			archivofactura:archivofacturaM,
          } 
        }).done(function(){  
			//alert(resp);                
          location.reload();
        });
    });
</script>
@endsection