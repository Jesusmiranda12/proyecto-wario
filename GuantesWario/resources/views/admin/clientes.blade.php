@extends('template2')

@section('contenido')
<section class="full-width pageContent">
		<section class="full-width header-well">
			<div class="full-width header-well-icon">
				<i class="zmdi zmdi-accounts"></i>
			</div>
			<div class="full-width header-well-text">
				<p class="text-condensedLight">
					Lista de los clientes activos y clientes por cotización. En caso de no haber realizado la venta, Eliminar cliente.
				</p>
			</div>
		</section>
		<div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
			<div class="mdl-tabs__tab-bar">
				
				<a href="#tabListClient" class="mdl-tabs__tab">Lista de clientes</a>
			</div>
			
			<div class="mdl-tabs__panel" id="tabListClient">
				<div class="mdl-grid">
					<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--12-col-desktop">
						<div class="full-width panel mdl-shadow--2dp">
							<div class="full-width panel-tittle bg-success text-center tittles">
								Lista de Clientes
							</div>
							<div class="full-width panel-content">
								<form action="#">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
										<label class="mdl-button mdl-js-button mdl-button--icon" for="searchClient">
											<i class="zmdi zmdi-search"></i>
										</label>
										<div class="mdl-textfield__expandable-holder">
											<input class="mdl-textfield__input" type="text" id="searchClient">
											<label class="mdl-textfield__label"></label>
										</div>
									</div>
								</form>
								<div class="container">
									<table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp full-width table-responsive">
										<thead>
											<tr>
												<th>ID</th>
												<th>NOMBRE</th>
												<th>E-MAIL</th>
												<th>RAZÓN SOCIAL</th>
												<th>RFC</th>
												<th>DIRECCIÓN</th>
												<th>TELEFONO</th>
												<th>ACCION</th>
											</tr>
										</thead>
										<tbody>
											@foreach($clientes as $cliente)
											<tr class="mdl-data-table tbody tr:hover">
											<td>{{$cliente->id}}</td>
											<td>{{$cliente->nombreC}}</td>
											<td>{{$cliente->email}}</td>
											<td>{{$cliente->razon}}</td>
											<td>{{$cliente->rfc}}</td>
											<td>{{$cliente->direccion}}</td>
											<td>{{$cliente->telefono}}</td>
											<td> 
												<div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                                	<form action="{{url('/clientes/'.$cliente->id)}}" class="formulario-eliminar" method="POST">
                                                	@csrf  
                                                	@method('DELETE')
                                                	<button class="btn btn-danger" type="submit"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16">
                                                	<path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
                                                	</svg></button>
                                                	</form>
                                                </div>
												</div>
											</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection