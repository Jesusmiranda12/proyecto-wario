@extends('templateEditar')

@section('contenido')
<section class="full-width pageContent">
		<section class="full-width header-well">
			<div class="full-width header-well-icon">
				<i class="zmdi zmdi-washing-machine"></i>
			</div>
			<div class="full-width header-well-text">
				<p class="text-condensedLight">
					Actualiza datos del producto en el catálago de productos.
				</p>
			</div>
		</section>
		<div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
			<div class="mdl-tabs__tab-bar">
				<a href="#tabNewProduct" class="mdl-tabs__tab is-active">Datos del producto</a>
				
			</div>
			<div class="mdl-tabs__panel is-active" id="tabNewProduct">
				<div class="mdl-grid">
					<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--12-col-desktop">
						<div class="full-width panel mdl-shadow--2dp">
							<div class="full-width panel-tittle bg-primary text-center tittles">
								Actualizar Producto
							</div>
							<div class="full-width panel-content">
								<form action="{{route('productos.update', $productos->id)}}" method="post" enctype="multipart/form-data">
								@csrf
                                @method('PATCH')
									<div class="mdl-grid">
										<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--6-col-desktop">
											<h5 class="text-condensedLight">Información básica</h5>
											
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
											    <label for="nombreP" >Nombre</label>
												<input value="{{$productos->nombreP}}" class="mdl-textfield__input" type="text" pattern="-?[A-Za-z0-9áéíóúÁÉÍÓÚ ]*(\.[0-9]+)?" id="nombreP" name="nombreProducto">
												
												<span class="mdl-textfield__error">Nombre inválido</span>
											</div>
											<div class="mdl-textfield mdl-js-textfield">
												<select value="{{$productos->categoria}}" class="mdl-textfield__input" id="categoriaP" name="categoriaProducto">
													<option>Seleccionar categoría</option>
													@foreach($categorias as $categoria)
													<option>{{$categoria->nombre}}</option>
													@endforeach
												</select>
											</div>
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
											<label for="precioP">Precio</label>
												<input value="{{$productos->precio}}" class="mdl-textfield__input" type="text" pattern="-?[0-9.]*(\.[0-9]+)?" id="precioP" name="precioProducto">
												
												<span class="mdl-textfield__error">Precio inválido</span>
											</div>
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
											<label for="tallaP">Talla</label>
												<input value="{{$productos->talla}}" class="mdl-textfield__input" type="text" pattern="-?[A-Za-z0-9áéíóúÁÉÍÓÚ ]*(\.[0-9]+)?" id="tallaP" name="tallaProducto">
												
												<span class="mdl-textfield__error">Talla inválida</span>
											</div>	
										</div>
										<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--6-col-desktop">
											<h5 class="text-condensedLight">Agrega una imágen del producto</h5>
											<div class="mdl-textfield mdl-js-textfield">
												<input value="{{$productos->imagen}}" type="file" name="imagen">
											</div>
											
										</div>
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<label for="descripcionP">Descripción</label>
												<input value="{{$productos->descripcion}}" class="mdl-textfield__input" type="text" pattern="-?[A-Za-z0-9áéíóúÁÉÍÓÚ ]*(\.[0-9]+)?" id="descripcionP" name="descripcionProducto">
												
												<span class="mdl-textfield__error">Descripcion invalida</span>
										</div>
									</div>
									<p class="text-center">
										<button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored bg-primary" id="btn-addProduct">
											<i class="zmdi zmdi-plus"></i>
										</button>
										<div class="mdl-tooltip" for="btn-addProduct">Actualizar producto</div>
									</p>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
</section>
@endsection