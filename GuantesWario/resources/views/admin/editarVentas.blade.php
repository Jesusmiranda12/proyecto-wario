@extends('templateEditar')

@section('contenido')
<section class="full-width pageContent">
		<section class="full-width header-well">
			<div class="full-width header-well-icon">
				<i class="zmdi zmdi-shopping-cart"></i>
			</div>
			<div class="full-width header-well-text">
				<p class="text-condensedLight">
					Registra las ventas realizadas y Consulta los detalles de la venta.
				</p>
			</div>
		</section>
		<div class="full-width divider-menu-h"></div>
		<div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
			<div class="mdl-tabs__tab-bar">
				<a href="#tabNewVenta" class="mdl-tabs__tab is-active">Registrar Venta</a>
				<a href="#tabListVentas" class="mdl-tabs__tab">Consultar Ventas</a>
			</div>
			<div class="mdl-tabs__panel is-active" id="tabNewVenta">
				<div class="mdl-grid">
					<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--12-col-desktop">
						<div class="full-width panel mdl-shadow--2dp">
							<div class="full-width panel-tittle bg-primary text-center tittles">
								Registrar venta
							</div>
							<div class="full-width panel-content">
								<form action="{{route('ventas.update', $ventas->id)}}" method="post" enctype="multipart/form-data">
								@csrf
                                @method('PATCH')
									<div class="mdl-grid">
										<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--6-col-desktop">
											<h5 class="text-condensedLight">Información básica</h5>
											
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
												<input value="{{$ventas->fecha}}" class="mdl-textfield__input"  id="fechaV" name="fechaVenta">
												<label class="mdl-textfield__label" for="fechaV">Fecha</label>
												<span class="mdl-textfield__error">Fecha no valida</span>
											</div>
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
												<input value="{{$ventas->nofactura}}" class="mdl-textfield__input" type="text" pattern="-?[A-Za-z0-9áéíóúÁÉÍÓÚ ]*(\.[0-9]+)?" id="facturaV" name="facturaVenta">
												<label class="mdl-textfield__label" for="facturaV">Número de factura</label>
												<span class="mdl-textfield__error">Dato incorrecto</span>
											</div>
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
												<input value="{{$ventas->razonsocial}}" class="mdl-textfield__input" type="text" pattern="-?[A-Za-z0-9áéíóúÁÉÍÓÚ ]*(\.[0-9]+)?" id="razonV" name="razonVenta">
												<label class="mdl-textfield__label" for="razonV">Razón Social</label>
												<span class="mdl-textfield__error">Dato incorrecto</span>
											</div>
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
												<input value="{{$ventas->noguia}}" class="mdl-textfield__input" type="text" pattern="-?[A-Za-z0-9áéíóúÁÉÍÓÚ ]*(\.[0-9]+)?" id="numeroV" name="numeroVenta">
												<label class="mdl-textfield__label" for="numeroV">Número de guía</label>
												<span class="mdl-textfield__error">Datos no válidos</span>
											</div>
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
												<label class="mdl-textfield__label" for="descripcionV">Descripción</label>
												<textarea value="{{$ventas->descripcion}}" class="mdl-textfield__input" type="text" pattern="-?[A-Za-z0-9áéíóúÁÉÍÓÚ ]*(\.[0-9]+)?" id="descripcionV" name="descripcionVenta">
												</textarea>
												
												<span class="mdl-textfield__error">Dato incorrecto</span>
											</div>
												
										</div>
										<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--6-col-desktop">
											<h5 class="text-condensedLight">Archivo de la guía</h5>
											<div class="mdl-textfield mdl-js-textfield">
												<input value="{{$ventas->archivoguia}}" type="file" name="archivoguiaVenta">
											</div>
											<h5 class="text-condensedLight">Archivo de factura</h5>
											<div class="mdl-textfield mdl-js-textfield">
												<input value="{{$ventas->archivofactura}}" type="file" name="archivofacturaVenta">
											</div>
										</div>
									</div>
									<p class="text-center">
										<button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored bg-primary" id="btn-addProduct">
											<i class="zmdi zmdi-plus"></i>
										</button>
										<div class="mdl-tooltip" for="btn-addProduct">Añadir producto</div>
									</p>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
     </div>		
</section>
@endsection