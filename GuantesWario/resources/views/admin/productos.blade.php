@extends('template2')

@section('contenido')
<section class="full-width pageContent">
		<section class="full-width header-well">
			<div class="full-width header-well-icon">
				<i class="zmdi zmdi-washing-machine"></i>
			</div>
			<div class="full-width header-well-text">
				<p class="text-condensedLight">
					Agrega nuevos productos para que tus clientes puedan verlos en el catálago de productos.
				</p>
			</div>
		</section>
		<div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
			<div class="mdl-tabs__tab-bar">
				<a href="#tabNewProduct" class="mdl-tabs__tab is-active">Nuevo producto</a>
				<a href="#tabListProducts" class="mdl-tabs__tab">Lista de productos</a>
			</div>
			<div class="mdl-tabs__panel is-active" id="tabNewProduct">
				<div class="mdl-grid">
					<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--12-col-desktop">
						<div class="full-width panel mdl-shadow--2dp">
							<div class="full-width panel-tittle bg-primary text-center tittles">
								Nuevo Producto
							</div>
							<div class="full-width panel-content">
								<form action="{{route('productos.store')}}" method="post" enctype="multipart/form-data">
								@csrf
									<div class="mdl-grid">
										<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--6-col-desktop">
											<h5 class="text-condensedLight">Información básica</h5>
											
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
												<input class="mdl-textfield__input" type="text" pattern="-?[A-Za-z0-9áéíóúÁÉÍÓÚ ]*(\.[0-9]+)?" id="nombreP" name="nombreProducto">
												<label class="mdl-textfield__label" for="nombreP">Nombre</label>
												<span class="mdl-textfield__error">Nombre inválido</span>
											</div>
											<div class="mdl-textfield mdl-js-textfield">
												<select class="mdl-textfield__input" id="categoriaP" name="categoriaProducto">
													<option>Seleccionar categoría</option>
													@foreach($categorias as $categoria)
													<option>{{$categoria->nombre}}</option>
													@endforeach
												</select>
											</div>
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
												<input class="mdl-textfield__input" type="text" pattern="-?[0-9.]*(\.[0-9]+)?" id="precioP" name="precioProducto">
												<label class="mdl-textfield__label" for="precioP">Precio</label>
												<span class="mdl-textfield__error">Precio inválido</span>
											</div>
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
												<input class="mdl-textfield__input" type="text" pattern="-?[A-Za-z0-9áéíóúÁÉÍÓÚ ]*(\.[0-9]+)?" id="tallaP" name="tallaProducto">
												<label class="mdl-textfield__label" for="tallaP">Talla</label>
												<span class="mdl-textfield__error">Talla inválida</span>
											</div>	
										</div>
										<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--6-col-desktop">
											<h5 class="text-condensedLight">Agrega una imágen del producto</h5>
											<div class="mdl-textfield mdl-js-textfield">
												<input type="file" name="imagen">
											</div>
											
										</div>
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
												<input class="mdl-textfield__input" type="text" pattern="-?[A-Za-z0-9áéíóúÁÉÍÓÚ ]*(\.[0-9]+)?" id="descripcionP" name="descripcionProducto">
												<label class="mdl-textfield__label" for="descripcionP">Descripción</label>
												<span class="mdl-textfield__error">Descripcion invalida</span>
										</div>
									</div>
									<p class="text-center">
										<button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored bg-primary" id="btn-addProduct">
											<i class="zmdi zmdi-plus"></i>
										</button>
										<div class="mdl-tooltip" for="btn-addProduct">Añadir producto</div>
									</p>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="mdl-tabs__panel" id="tabListProducts">
				<div class="mdl-grid">
					<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--12-col-desktop">
						<form action="#">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
								<label class="mdl-button mdl-js-button mdl-button--icon" for="searchProduct">
									<i class="zmdi zmdi-search"></i>
								</label>
								<div class="mdl-textfield__expandable-holder">
									<input class="mdl-textfield__input" type="text" id="searchProduct">
									<label class="mdl-textfield__label"></label>
								</div>
							</div>
						</form>
						<nav class="full-width menu-categories">
						<div class="mdl-textfield mdl-js-textfield">
						<form action="{{route('productos.index',$categoria->nombre)}}">
							<select class="mdl-textfield__input" id="categoriaP" name="categoriaProducto">
								<option>Seleccionar categoría</option>
								@foreach($categorias as $categoria)
								<option>{{$categoria->nombre}}</option>
								@endforeach
							</select>
							<button type="submit" class="btn btn-success">Ver productos</button>
                        </form>
						</div>
						</nav>
						@foreach($productos as $producto)
						<div class="full-width text-center" style="padding: 30px 0;">
						
							<div class="mdl-card mdl-shadow--2dp full-width product-card">
							
								<div class="mdl-card__title">
									<img src="img/Productos/{{$producto->imagen}}" alt="product" class="img-responsive">
								</div>
								<div class="mdl-card__supporting-text">
									<small>Talla:{{$producto->talla}}</small><br>
									<small>{{$producto->precio}}</small>
								</div>
								<div class="mdl-card__actions mdl-card--border">
								{{$producto->nombreP}}
									<button class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect">
										<i class="zmdi zmdi-more"></i>
									</button>
								</div>
								<div class="row">
									<div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                    <a href="{{url('/productos/'.$producto->id.'/edit')}}" class="btn text-white" style="background-color: #207671;"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-file-earmark-diff-fill" viewBox="0 0 16 16">
                                    <path d="M9.293 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V4.707A1 1 0 0 0 13.707 4L10 .293A1 1 0 0 0 9.293 0zM9.5 3.5v-2l3 3h-2a1 1 0 0 1-1-1zM8 6a.5.5 0 0 1 .5.5V8H10a.5.5 0 0 1 0 1H8.5v1.5a.5.5 0 0 1-1 0V9H6a.5.5 0 0 1 0-1h1.5V6.5A.5.5 0 0 1 8 6zm-2.5 6.5A.5.5 0 0 1 6 12h4a.5.5 0 0 1 0 1H6a.5.5 0 0 1-.5-.5z"/>
                                    </svg></a>
                             	</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                	<form action="{{url('/productos/'.$producto->id)}}" class="formulario-eliminar" method="POST">
                                	@csrf  
                                	@method('DELETE')
                                	<button class="btn btn-danger" type="submit"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16">
                                	<path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
                                	</svg></button>
                                	</form>
                                </div>
												
					   
							</div>
							
						</div>
						@endforeach			
									
						
					</div>
				</div>
			</div>
		</div>
	</section>
<!-- Modal -->
<div class="modal fade" id="editarModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Actualizar datos</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
	  <div class="full-width panel-content">
			<form id="formularioEdit"  enctype="multipart/form-data" method="post">
			@csrf
			
				<input type="hidden" id="idModal">
				<h5 class="text-condensedLight">Datos del producto</h5>
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
				    <label for="nombreP">Nombre</label>
					<input class="mdl-textfield__input" type="text" pattern="-?[A-Za-z0-9áéíóúÁÉÍÓÚ ]*(\.[0-9]+)?" id="nombreModal" name="nombreProductoModal">	
					<span class="mdl-textfield__error">Nombre inválido</span>
				</div>
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
				    <label for="categoriaP">Categoria</label>
					<input class="mdl-textfield__input" type="text" pattern="-?[A-Za-z0-9áéíóúÁÉÍÓÚ ]*(\.[0-9]+)?" id="categoriaModal" name="categoriaProductoModal">
					
					<span class="mdl-textfield__error">Nombre inválido</span>
				</div>
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
					<label for="precioP">Precio</label>
					<input class="mdl-textfield__input" type="text" pattern="-?[0-9.]*(\.[0-9]+)?" id="precioModal" name="precioProductoModal">
					
					<span class="mdl-textfield__error">Precio inválido</span>
				</div>
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
					<label for="tallaP">Talla</label>
					<input class="mdl-textfield__input" type="text" pattern="-?[A-Za-z0-9áéíóúÁÉÍÓÚ ]*(\.[0-9]+)?" id="tallaModal" name="tallaProductoModal">
					
					<span class="mdl-textfield__error">Talla inválida</span>
				</div>	
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
				<label for="descripcionP">Descripción</label>
					<input class="mdl-textfield__input" type="text" pattern="-?[A-Za-z0-9áéíóúÁÉÍÓÚ ]*(\.[0-9]+)?" id="descripcionPModal" name="descripcionProductoModal">
					
					<span class="mdl-textfield__error">Descripcion invalida</span>
				</div>
				<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--6-col-desktop">
					<h5 class="text-condensedLight">Imágen del producto</h5>
					<div class="mdl-textfield mdl-js-textfield">
						
						<input class="mdl-textfield__input" type="text" id="imagenPModal" name="imagenPPModal">
						<input type="file" id="imagenModal" name="imagenProductoModal">
					</div>		
				</div>				
				<button type="submit" class="btn btn-primary">Guardar Cambios</button>			
			</form>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
        
      </div>
    </div>
  </div>
</div>
@endsection
@section('javascript')
<script>
	function editar(id)
	{
		$(document).ready(function(){
            $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
            $.ajax({
            url:'/editar/productos',
            method:'POST',
            data:{
            id:id
            } 
            }).done(function(respuesta){
				//alert(respuesta);
		    var arreglo = JSON.parse(respuesta);
			
			$("#idModal").val(arreglo[0].id);
			$("#nombreModal").val(arreglo[0].nombreP);
			$("#categoriaModal").val(arreglo[0].categoria);
			$("#precioModal").val(arreglo[0].precio);
			$("#tallaModal").val(arreglo[0].talla);
			$("#imagenPModal").val(arreglo[0].imagen);
			$("#descripcionPModal").val(arreglo[0].descripcion);
            });
        });
	}
</script>
<script>
    $('#formularioEdit').submit(function(e){
          e.preventDefault();
          $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        var idM = $('#idModal').val();
        var nombreM = $('#nombreModal').val();
        var categoriaM = $('#categoriaModal').val();
		var precioM = $('#precioModal').val();
		var tallaM = $('#tallaModal').val();
		var imagenM = $('#imagenModal').val();
		var descripcionM = $('#descripcionModal').val();
        $.ajax({
          url:'/actualizar/productos',
		  method:'POST',
          data:{
            id:idM,
            nombre:nombreM,
			categoria:categoriaM,
			precio:precioM,
			talla:tallaM,
			imagen:imagenM,
			descripcion:descripcionM,
          } 
        }).done(function(e){                  
          //alert(e);
		  location.reload();
        });
    });
</script>
@endsection