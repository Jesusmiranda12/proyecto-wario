@extends('template2')

@section('contenido')
<section class="full-width pageContent">
		<section class="full-width header-well">
			<div class="full-width header-well-icon">
				<i class="zmdi zmdi-label"></i>
			</div>
			<div class="full-width header-well-text">
				<p class="text-condensedLight">
					Agrega las categorías de los productos disponibles.
				</p>
			</div>
		</section>
		<div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
			<div class="mdl-tabs__tab-bar">
				<a href="#tabNewCategory" class="mdl-tabs__tab is-active">Nueva categoría</a>
				<a href="#tabListCategory" class="mdl-tabs__tab">Lista de categorías</a>
			</div>
			<div class="mdl-tabs__panel is-active" id="tabNewCategory">
				<div class="mdl-grid">
					<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--8-col-desktop mdl-cell--2-offset-desktop">
						<div class="full-width panel mdl-shadow--2dp">
							<div class="full-width panel-tittle bg-primary text-center tittles">
								Nueva categoría
							</div>
							<div class="full-width panel-content">
								<form action="{{route('categorias.store')}}" method="post">
									@csrf
									<h5 class="text-condensedLight">Datos de la categoría</h5>
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<input class="mdl-textfield__input" type="text" pattern="-?[A-Za-z0-9áéíóúÁÉÍÓÚ ]*(\.[0-9]+)?" id="Nombre" name="NombreCategoria">
										<label class="mdl-textfield__label" for="Nombre">Nombre</label>
										<span class="mdl-textfield__error">Nombre no válido</span>
									</div>
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<input class="mdl-textfield__input" type="text" pattern="-?[A-Za-záéíóúÁÉÍÓÚ ]*(\.[0-9]+)?" id="Descripcion" name="DescripcionCategoria">
										<label class="mdl-textfield__label" for="Descripcion">Descripción</label>
										<span class="mdl-textfield__error">Descripción no valida</span>
									</div>
									<p class="text-center">
										<button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored bg-primary" id="btn-addCategory" type="submit">
											<i class="zmdi zmdi-plus"></i>
										</button>
										<div class="mdl-tooltip" for="btn-addCategory">Añadir categoría</div>
									</p>
								</form>
							</div>

						</div>
					</div>
				</div>
			</div>
			<div class="mdl-tabs__panel" id="tabListCategory">
				<div class="mdl-grid">
					<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--8-col-desktop mdl-cell--2-offset-desktop">
						<div class="full-width panel mdl-shadow--2dp">
							<div class="full-width panel-tittle bg-success text-center tittles">
								Lista de categorías
							</div>
							<div class="full-width panel-content">
								<form action="#">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
										<label class="mdl-button mdl-js-button mdl-button--icon" for="searchCategory">
											<i class="zmdi zmdi-search"></i>
										</label>
										<div class="mdl-textfield__expandable-holder">
											<input class="mdl-textfield__input" type="text" id="searchCategory">
											<label class="mdl-textfield__label"></label>
										</div>
									</div>
								</form>
								<div class="container">
									<table class="table table-bordered table-responsive">
										<thead>
											<tr>
												<th>ID</th>
												<th>NOMBRE</th>
												<th>DESCRIPCIÓN</th>
												<th>OPCION</th>
											</tr>
										</thead>
										<tbody>
											@foreach($categorias as $categoria)
											<tr class="mdl-data-table tbody tr:hover">
											<td>{{$categoria->id}}</td>
											<td>{{$categoria->nombre}}</td>
											<td>{{$categoria->descripcion}}</td>
											<td> 
											    <div class="row">
													<div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                	                <button type="text" data-bs-toggle="modal" data-bs-target="#editarModal" onclick="editar({{$categoria->id}})" class="btn text-white" style="background-color: #207671;"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-file-earmark-diff-fill" viewBox="0 0 16 16">
                                	                <path d="M9.293 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V4.707A1 1 0 0 0 13.707 4L10 .293A1 1 0 0 0 9.293 0zM9.5 3.5v-2l3 3h-2a1 1 0 0 1-1-1zM8 6a.5.5 0 0 1 .5.5V8H10a.5.5 0 0 1 0 1H8.5v1.5a.5.5 0 0 1-1 0V9H6a.5.5 0 0 1 0-1h1.5V6.5A.5.5 0 0 1 8 6zm-2.5 6.5A.5.5 0 0 1 6 12h4a.5.5 0 0 1 0 1H6a.5.5 0 0 1-.5-.5z"/>
                                	                </svg></button>
                             					</div>
												<div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                                	<form action="{{url('/categorias/'.$categoria->id)}}" class="formulario-eliminar" method="POST">
                                                	@csrf  
                                                	@method('DELETE')
                                                	<button class="btn btn-danger" type="submit"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16">
                                                	<path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
                                                	</svg></button>
                                                	</form>
                                                </div>
												</div>
											</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Modal -->
<div class="modal fade" id="editarModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Actualizar datos</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
	  <div class="full-width panel-content">
			<form id="formularioEditar">
				@csrf
				<input type="hidden" id="idModal">
				<h5 class="text-condensedLight">Datos de la categoría</h5>
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
				    <label  for="Nombre">Nombre</label>
					<input class="mdl-textfield__input" type="text" pattern="-?[A-Za-z0-9áéíóúÁÉÍÓÚ ]*(\.[0-9]+)?" id="nombreModal" name="NombreCategoriaModal">	
					<span class="mdl-textfield__error">Nombre no válido</span>
					
				</div>
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
				    <label for="Descripcion">Descripción</label>
					<input class="mdl-textfield__input" type="text" pattern="-?[A-Za-záéíóúÁÉÍÓÚ ]*(\.[0-9]+)?" id="descripcionModal" name="DescripcionCategoriaModal">		
					<span class="mdl-textfield__error">Descripción no valida</span>
				</div>
				<button type="submit" class="btn btn-primary">Guardar Cambios</button>
				
			</form>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
        
      </div>
    </div>
  </div>
</div>
@endsection
@section('javascript')
<script>
	function editar(id)
	{
		$(document).ready(function(){
            $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
            $.ajax({
            url:'/editar/categorias',
            method:'POST',
            data:{
            id:id
            } 
            }).done(function(respuesta){

		    var arreglo = JSON.parse(respuesta);
			
			$("#idModal").val(arreglo[0].id);
			$("#nombreModal").val(arreglo[0].nombre);
			$("#descripcionModal").val(arreglo[0].descripcion);
            });
        });
	}
</script>
<script>
    $('#formularioEditar').submit(function(e){
          e.preventDefault();
          $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        var idM = $('#idModal').val();
        var nombreM = $('#nombreModal').val();
        var descripcionM = $('#descripcionModal').val();
        $.ajax({
          url:'/actualizar/categorias',
          method:'POST',
          data:{
            id:idM,
            nombre:nombreM,
			descripcion:descripcionM,
          } 
        }).done(function(){                  
          location.reload();
        });
    });
</script>
@endsection