<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ventas', function (Blueprint $table) {
            $table->id();
            $table->string('fecha');
            $table->string('nofactura');
            $table->string('razonsocial');
            $table->string('producto');
            $table->decimal('precioproducto',9,2);
            $table->integer('cantidad');
            $table->decimal('total',9,2);
            $table->string('noguia');
            $table->string('archivoguia');
            $table->string('archivofactura');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ventas');
    }
}
