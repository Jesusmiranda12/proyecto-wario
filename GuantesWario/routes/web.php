<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('informacion','InformacionCtrl');
Route::resource('tienda','TiendaCtrl');
Route::get('tiendaCategoria/{id}','TiendaCtrl@index');
Route::resource('contacto','ContactoCtrl');
Route::resource('carrito','CarritoCtrl');
Route::resource('loginAdmin','loginAdminCtrl');
Route::resource('inicioAdmin','inicioAdminCtrl');
Route::resource('pagos','pagosCtrl');
Route::resource('categorias','categoriasCtrl');
//URL para editar categorias con metodo Ajax.
Route::post('/editar/categorias','categoriasCtrl@editarCategoria');
//URL para actualizar datos
Route::post('/actualizar/categorias','categoriasCtrl@actualizarCategoria');
Route::resource('administradores','administradoresCtrl');
Route::resource('clientes','clientesCtrl');
Route::resource('productos','productosCtrl');
//URL para editar productos con metodo Ajax.
Route::post('/editar/productos','productosCtrl@editarProducto');
//URL para actualizar datos de productos
Route::post('/actualizar/productos','productosCtrl@actualizarProducto');
Route::resource('ventas','ventasCtrl');
//URL para editar ventas con metodo Ajax.
Route::post('/editar/ventas','ventasCtrl@editarVenta');
//URL para actualizar datos de venta
Route::post('/actualizar/ventas','ventasCtrl@actualizarVenta');