<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;
use App\Categoria;
use Illuminate\Support\Str;
use File;

class productosCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        /*$productos = Producto::all();
        $categorias = Categoria::all();
        return view('admin.productos',compact('productos','categorias'));*/
        //
        $categorias = Categoria::all();

        if($request)
        {
        //dd($request);
         $query=trim($request->get('categoriaProducto'));
         $productos = Producto::where('categoria','LIKE','%'.$query.'%')->get();
         return view('admin.productos',compact('productos','categorias'));
        } 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //dd($request);
        $datos = new Producto;
        $datos->nombreP = $request->nombreProducto;
        $datos->categoria = $request->categoriaProducto;
        $datos->precio = $request->precioProducto;
        $datos->talla = $request->tallaProducto;
        $datos->imagen = $request->imagen;
        $datos->descripcion = $request->descripcionProducto;
        if($request->hasfile("imagen"))
        {  
            $imagen=$request->file("imagen");  
            $nombreImagen = str::slug($request->nombreProducto).".".$imagen->guessExtension(); 
            $ruta = public_path("img/Productos/");
            $imagen->move($ruta,$nombreImagen);
            $datos->imagen = $nombreImagen;
        }
        if($datos->save())
        {
            return redirect('productos');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        //dd($id);
        $productos = Producto::find($id);
        $categorias = Categoria::all();
        return view('admin.editarProductos',compact('productos','categorias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //dd($request);
        $datos = Producto::find($id);
        $datos->nombreP = $request->nombreProducto;
        $datos->categoria = $request->categoriaProducto;
        $datos->precio = $request->precioProducto;
        $datos->talla = $request->tallaProducto;
        //$datos->imagen = $request->imagen;
        $datos->descripcion = $request->descripcionProducto;
        //$datos->update($request->imagen);
        if($request->hasfile("imagen"))
        {  
            $destinationPath = 'img/Productos';
            File::delete($destinationPath.'/'.$datos->imagen);
            $imagen=$request->file("imagen");  
            $nombreImagen = str::slug($request->nombreProducto).".".$imagen->guessExtension(); 
            $ruta = public_path("img/Productos/");
            $imagen->move($ruta,$nombreImagen);
            //$datos->update($datos->imagen = $imagen);
            $datos->imagen = $nombreImagen;
        }
        if($datos->save())
        {
            return redirect('productos');
        }
    }

    public function editarProducto(Request $request)
    {
        //Consulta para obtener datos
        $producto = Producto::find($request->id);
        $datos = Producto::where('id','=', $producto->id)->get(); 
        return response(json_encode($datos),200)->header('Content-type','text/plain');
    }
    public function actualizarProducto(Request $request)
    {
        //dd($request->imagen);
        $datos = Producto::find($request->id);
        $datos->nombreP = $request->nombreProducto;
        $datos->categoria = $request->categoriaProducto;
        $datos->precio = $request->precioProducto;
        $datos->talla = $request->tallaProducto;
        $datos->imagen = $request->imagen;
        $datos->descripcion = $request->descripcionProducto;
        //$datos->update($request->imagen);
        if($request->hasfile("imagen"))
        {  
            $destinationPath = 'img/Productos';
            File::delete($destinationPath.'/'.$request->imagen);
            $imagen=$request->file("imagen");  
            $nombreImagen = str::slug($request->nombreProducto).".".$imagen->guessExtension(); 
            $ruta = public_path("img/Productos/");
            $imagen->move($ruta,$nombreImagen);
            //$datos->update($datos->imagen = $imagen);
            $datos->imagen = $nombreImagen;
        }
        if($datos->save())
        {
            return redirect('productos');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $datos=Producto::where('id','=',$id)->first();
        $destinationPath = 'img/Productos';
        File::delete($destinationPath.'/'.$datos->imagen);
        Producto::destroy($id);
        return redirect('productos');
    }
}
