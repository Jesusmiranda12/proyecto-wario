<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Venta;
use Illuminate\Support\Str;
use Carbon\Carbon;

class ventasCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $ventas = Venta::all();
        return view('admin.ventas',['ventas'=>$ventas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $datos = new Venta;
        $datos->fecha = $request->fechaVenta;
        $datos->nofactura = $request->facturaVenta;
        $datos->razonsocial = $request->razonVenta;
        $datos->descripcion = $request->descripcionVenta;
        $datos->noguia = $request->numeroVenta;
        $datos->archivoguia = $request->archivoguiaVenta;
        $datos->archivofactura = $request->archivofacturaVenta;
        if($request->hasfile("archivoguiaVenta"))
                {
                    $file=$request->file("archivoguiaVenta");
                    $now = Carbon::now()->toDateString();
                    $nombreArchivo ="pdf_".$now.".".$file->guessExtension();
    
                    $ruta = public_path("pdf/guias/".$nombreArchivo);
    
                    if($file->guessExtension()=="pdf")
                    {
                        copy($file,$ruta);
                    }
                    else{
                        dd("No es un pdf");
                    }
    
                    $datos->archivoguia = $nombreArchivo;
                }
        /*if($request->hasfile("archivoguiaVenta"))
        {  
            $archivo=$request->file("archivoguiaVenta");  
            $nombreArchivo = str::slug($request->numeroVenta).".".$archivo->guessExtension(); 
            $ruta = public_path("pdf/guias/");
            $archivo->move($ruta,$nombreArchivo);
            $datos->archivoguia = $nombreArchivo;
        }*/
        if($request->hasfile("archivofacturaVenta"))
        {  
            $archivo=$request->file("archivofacturaVenta");  
            $nombreArchivo = str::slug($request->facturaVenta).".".$archivo->guessExtension(); 
            $ruta = public_path("pdf/facturas/");
            $archivo->move($ruta,$nombreArchivo);
            $datos->archivofactura = $nombreArchivo;
        }
        if($datos->save())
        {
            return redirect('ventas');
        }
    }

    public function editarVenta(Request $request)
    {
        //Consulta para obtener datos
        $venta = Venta::find($request->id);
        $datos = Venta::where('id','=', $venta->id)->get(); 
        return response(json_encode($datos),200)->header('Content-type','text/plain');
    }
    public function actualizarVenta(Request $request)
    {
        
        $datos = Venta::find($request->id);
        $datos->fecha = $request->fecha;
        $datos->nofactura = $request->nofactura;
        $datos->razonsocial = $request->razonsocial;
        $datos->descripcion = $request->descripcion;
        $datos->noguia = $request->noguia;
        $datos->archivoguia= $request->archivoguia;
        if($request->hasfile("archivoguia"))
                {
                    $file=$request->file("archivoguia");
                    $now = Carbon::now()->toDateString();
                    $nombreArchivo = "pdf_".$now.".".$file->guessExtension();
                   
                    $ruta = public_path("pdf/guias/".$nombreArchivo);
    
                    if($file->guessExtension()=="pdf")
                    {
                        copy($file,$ruta);
                    }
                    else{
                        dd("No es un pdf");
                    }
    
                    $datos->archivoguia = $nombreArchivo;
                }
        if($request->hasfile("archivofactura"))
        {
           
            $destinationPath = 'pdf/facturas/';
            File::delete($destinationPath.'/'.$datos->archivofactura);
            $archivo=$request->file("archivofacturaVenta");  
            $nombreArchivo = str::slug($request->nofactura).".".$archivo->guessExtension(); 
            $ruta = public_path("pdf/facturas/");
            $archivo->move($ruta,$nombreArchivo);
            $datos->archivoguia = $nombreArchivo;
        }
        
        if($datos->save())
        {
            return redirect('ventas');
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $ventas = Venta::find($id);
        //$categorias = Categoria::all();
        return view('admin.editarVentas',compact('ventas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $datos = Venta::find($id);
        $datos->fecha = $request->fechaVenta;
        $datos->nofactura = $request->facturaVenta;
        $datos->razonsocial = $request->razonVenta;
        $datos->descripcion = $request->descripcionVenta;
        $datos->noguia = $request->numeroVenta;
        //$datos->archivoguia = $request->archivoguiaVenta;
        //$datos->archivofactura = $request->archivofacturaVenta;
        if($request->hasfile("archivoguiaVenta"))
        {  
            $destinationPath = 'pdf/guias';
            File::delete($destinationPath.'/'.$datos->archivoguia);
            $archivoguia=$request->file("archivoguiaVenta");  
            $nombreArchivo = str::slug($request->noguia).".".$archivoguia->guessExtension(); 
            $ruta = public_path("pdf/guias/");
            $archivoguia->move($ruta,$nombreArchivo);
            //$datos->update($datos->imagen = $imagen);
            $datos->archivoguia = $nombreArchivo;
        }
        if($request->hasfile("archivofacturaVenta"))
        {  
            $destinationPath = 'pdf/facturas';
            File::delete($destinationPath.'/'.$datos->archivofactura);
            $archivofactura=$request->file("archivofacturaVenta");  
            $nombreArchivo = str::slug($request->nofactura).".".$archivofactura->guessExtension(); 
            $ruta = public_path("pdf/facturas/");
            $archivofactura->move($ruta,$nombreArchivo);
            //$datos->update($datos->imagen = $imagen);
            $datos->archivofactura = $nombreArchivo;
        }
        if($datos->save())
        {
            return redirect('ventas');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
       
        
        Venta::destroy($id);
        return redirect('ventas');
        
    }
}
