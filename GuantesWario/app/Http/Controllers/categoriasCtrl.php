<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;

class categoriasCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categorias = Categoria::all();
        return view('admin.categorias',['categorias'=>$categorias]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $datos = new Categoria;
        $datos->nombre = $request->NombreCategoria;
        $datos->descripcion = $request->DescripcionCategoria;
        if($datos->save())
        {
            return redirect('categorias');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function editarCategoria(Request $request)
    {
        //Consulta para obtener datos
        $categoria = Categoria::find($request->id);
        $datos = Categoria::where('id','=', $categoria->id)->get(); 
        return response(json_encode($datos),200)->header('Content-type','text/plain');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function actualizarCategoria(Request $request)
    {
        $datos = Categoria::find($request->id);
        $datos->nombre = $request->nombre;
        $datos->descripcion = $request->descripcion;
        if($datos->save())
        {
            return redirect('categorias');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Categoria::destroy($id);
        return redirect('categorias');
    }
}
