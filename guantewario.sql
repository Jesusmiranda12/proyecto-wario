create database guantewario;
use guantewario;
create table categorias(id int primary key auto_increment, nombre varchar(100), descripcion varchar(255), created_at timestamp, updated_at timestamp);
create table ventas(id int primary key auto_increment, fecha varchar(100), nofactura varchar(50), razonsocial varchar(50), descripcion varchar(255), noguia varchar(50), archivoguia varchar(255), archivofactura varchar(255), created_at timestamp, updated_at timestamp);
create table productos(id int primary key auto_increment, nombreP varchar(100), categoria varchar(100), precio decimal(9,2), talla varchar(100), imagen varchar(100), descripcion varchar(255), created_at timestamp, updated_at timestamp);
create table users(id int primary key auto_increment, name varchar(255), email varchar(255), email_verified_at timestamp, password varchar(255), remember_token varchar(100), created_at timestamp, updated_at timestamp);
create table password_resets(email varchar(255), token varchar(255), created_at timestamp);
create table failed_jobs(id int primary key auto_increment, connection text, queue text, payload longtext, exception longtext, failed_at timestamp);